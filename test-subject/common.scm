(define-module (test-subject common)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics viewport)
  #:use-module (chickadee math vector)
  #:use-module (oop goops)
  #:use-module (starling node-2d)
  #:use-module (starling scene)
  #:export (%window-width
            %window-height
            %game-width
            %game-height
            set-cameras!))

(define %window-width 1280)
(define %window-height 720)
(define %game-width 640)
(define %game-height 360)

(define-method (set-cameras! (scene <scene-2d>))
  (set! (cameras scene)
        (list (make <camera-2d>
                #:resolution (vec2 %game-width %game-height)
                #:viewport (make-viewport 0 0 %window-width %window-height
                                          #:clear-color black)))))
