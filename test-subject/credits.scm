(define-module (test-subject credits)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (starling kernel)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:use-module (starling scene)
  #:use-module (test-subject assets)
  #:use-module (test-subject common)
  #:export (<credits>))

(define-class <credits> (<scene-2d>)
  (true-ending? #:getter true-ending? #:init-keyword #:true-ending?
                #:init-value #f)
  (click-channel #:getter click-channel #:init-thunk make-channel))

(define-method (on-boot (credits <credits>))
  (set-cameras! credits)
  (attach-to credits
             (make <sprite>
               #:name 'background
               #:texture lightness)
             (make <label>
               #:name 'label
               #:font monogram-font
               #:color black
               #:align 'center
               #:vertical-align 'center
               #:position (vec2 (/ %game-width 2.0) (/ %game-height 2.0)))))

(define-method (on-enter (credits <credits>))
  (define* (credit line #:optional (sleep? #t))
    (set! (text (& credits label)) line)
    (when sleep? (sleep 120)))
  (run-script credits
    (set! (background-music credits) credits-music)
    (set! (background-music-loop? credits) #f)
    (sleep 60)
    (credit "The Test Subject")
    (credit "developed by David Thompson (GPLv3) https://dthompson.us")
    (credit "made for the Spring Lisp Game Jam 2021 https://itch.io/jam/spring-lisp-game-jam-2021")
    (credit "monogram font by datagoblin (CC0) https://datagoblin.itch.io/monogram")
    (credit "old fax font by George Blackwell (CC-BY 4.0) https://georgeblackwell.itch.io/old-fax")
    (credit "UI sounds by Kenney (CC0) https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks")
    (credit "background music by brandon75689 (CC0) https://opengameart.org/content/tragic-ambient-main-menu")
    (credit "credits music by tcarisland (CC-BY 4.0) https://opengameart.org/content/the-end")
    (if (true-ending? credits)
        (credit "congratulations on reaching the true ending!")
        (credit "the true ending still awaits you..."))
    (credit "click to play again" #f)
    (channel-clear! (click-channel credits))
    (channel-get (click-channel credits))
    (play-click-sound)
    (tween 120 white black
           (lambda (color)
             (set! (tint (& credits background)) color))
           #:interpolate color-lerp)
    (pop-scene (current-kernel))))

(define-method (on-mouse-release (credits <credits>) button x y)
  (when (eq? button 'left)
    (channel-put! (click-channel credits) #t)))
