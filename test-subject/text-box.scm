(define-module (test-subject text-box)
  #:use-module (chickadee graphics font)
  #:use-module (chickadee math vector)
  #:use-module (oop goops)
  #:use-module (starling asset)
  #:use-module (starling gui)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:duplicates (merge-generics replace warn-override-core warn last)
  #:export (<text-box>
            text-box-text))

(define-class <text-box> (<widget>)
  (text #:accessor text-box-text #:init-keyword #:text #:init-value "" #:watch? #t))

(define-method (on-change (text-box <text-box>) slot-name old new)
  (case slot-name
    ((text)
     (let ((l (& text-box text)))
       (when l
         (set! (text l) new))))
    (else
     (next-method))))

(define-method (apply-theme (text-box <text-box>))
  (next-method)
  (replace text-box
    (make <label>
      #:name 'text
      #:rank 1
      #:font (font text-box)
      #:text (text-box-text text-box)
      #:position (vec2 6.0
                       (- (height text-box)
                          (font-line-height
                           (asset-ref
                            (font text-box))))))))
