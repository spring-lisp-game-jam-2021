(define-module (test-subject assets)
  #:use-module (chickadee audio)
  #:use-module (chickadee graphics font)
  #:use-module (chickadee graphics texture)
  #:use-module (starling asset)
  #:export (dialog-box-texture
            button-press-texture
            darkness
            lightness
            room-background
            door-lock-texture
            pin-entry-background
            door-texture
            terminal-texture
            terminal-background
            control-panel-texture
            fridge-texture
            window-texture
            notebook-texture
            notebook-background
            monogram-font
            monogram-font-big
            old-fax-font
            click-sound
            key-press-sound
            device-sound
            gameplay-music
            credits-music
            play-click-sound
            play-key-press-sound
            play-device-sound))

(define (scope-datadir file-name)
  (let ((prefix (or (getenv "TEST_SUBJECT_DATADIR") (getcwd))))
    (string-append prefix "/" file-name)))

(define-asset dialog-box-texture (load-image (scope-datadir "assets/images/dialog-box.png")))
(define-asset button-press-texture (load-image (scope-datadir "assets/images/button-press.png")))
(define-asset darkness (load-image (scope-datadir "assets/images/darkness.png")))
(define-asset lightness (load-image (scope-datadir "assets/images/lightness.png")))
(define-asset room-background (load-image (scope-datadir "assets/images/room.png")))
(define-asset door-lock-texture (load-image (scope-datadir "assets/images/door-lock.png")))
(define-asset pin-entry-background (load-image (scope-datadir "assets/images/pin-entry-background.png")))
(define-asset door-texture (load-image (scope-datadir "assets/images/door.png")))
(define-asset terminal-texture (load-image (scope-datadir "assets/images/terminal.png")))
(define-asset terminal-background (load-image (scope-datadir "assets/images/terminal-background.png")))
(define-asset control-panel-texture (load-image (scope-datadir "assets/images/control-panel.png")))
(define-asset fridge-texture (load-image (scope-datadir "assets/images/fridge.png")))
(define-asset window-texture (load-image (scope-datadir "assets/images/window.png")))
(define-asset notebook-texture (load-image (scope-datadir "assets/images/notebook.png")))
(define-asset notebook-background (load-image (scope-datadir "assets/images/notebook-background.png")))
(define-asset monogram-font (load-font (scope-datadir "assets/fonts/monogram_extended.ttf") 12))
(define-asset monogram-font-big (load-font (scope-datadir "assets/fonts/monogram_extended.ttf") 24))
(define-asset old-fax-font (load-font (scope-datadir "assets/fonts/old-fax.ttf") 12))
(define-asset click-sound (load-audio (scope-datadir "assets/sounds/click1.wav")))
(define-asset key-press-sound (load-audio (scope-datadir "assets/sounds/switch6.wav")))
(define-asset device-sound (load-audio (scope-datadir "assets/sounds/switch38.wav")))
(define-asset gameplay-music (load-audio (scope-datadir "assets/sounds/ambientmain_0.ogg") #:mode 'stream))
(define-asset credits-music (load-audio (scope-datadir "assets/sounds/end-theme.mp3") #:mode 'stream))

(define (play-click-sound)
  (audio-play (asset-ref click-sound)))

(define (play-key-press-sound)
  (audio-play (asset-ref key-press-sound)))

(define (play-device-sound)
  (audio-play (asset-ref device-sound)))
