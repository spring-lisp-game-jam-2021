(define-module (test-subject splash)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (starling kernel)
  #:use-module (starling node)
  #:use-module (starling node-2d)
  #:use-module (starling scene)
  #:use-module (test-subject assets)
  #:use-module (test-subject common)
  #:use-module (test-subject game)
  #:export (launch-game))

(define-class <splash> (<scene-2d>))

(define-method (on-boot (splash <splash>))
  (set-cameras! splash)
  (attach-to splash
             (make <sprite>
               #:name 'background
               #:texture lightness)
             (make <label>
               #:name 'label
               #:font monogram-font
               #:color black
               #:align 'center
               #:vertical-align 'center
               #:position (vec2 (/ %game-width 2.0) (/ %game-height 2.0)))))

(define-method (on-enter (splash <splash>))
  (run-script splash
    (set! (text (& splash label)) "Nonexistent Game Studio sheepishly presents")
    (sleep 120)
    (set! (text (& splash label)) "The Test Subject - A Spring Lisp Game Jam 2021 submission")
    (sleep 120)
    (tween 120 white black
           (lambda (color)
             (set! (tint (& splash background)) color))
           #:interpolate color-lerp)
    (replace-scene (current-kernel) (make <game>))))

(define (launch-game)
  (boot-kernel (make <kernel>
                 #:window-config (make <window-config>
                                   #:title "The Test Subject - Spring Lisp Game Jam 2021"
                                   #:width %window-width
                                   #:height %window-height))
               (lambda () (make <splash>))))
