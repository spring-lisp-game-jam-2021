(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix build-system gnu)
             (guix download)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (gnu packages)
             (gnu packages audio)
             (gnu packages autotools)
             (gnu packages fontutils)
             (gnu packages gl)
             (gnu packages guile)
             (gnu packages image)
             (gnu packages maths)
             (gnu packages mp3)
             (gnu packages pkg-config)
             (gnu packages sdl)
             (gnu packages texinfo)
             (gnu packages xiph))

(define target-guile
  (package
    (inherit guile-3.0-latest)
    (version "3.0.6")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/guile/guile-"
                                  version ".tar.xz"))
              (sha256
               (base32
                "0a2xhjy6y5p15qvc59pdnzi20fm1jwqa772pwxb16wkx0z187gg2"))))
    (arguments
     `(#:tests? #f ,@(package-arguments guile-3.0-latest)))))

(define guile3.0-opengl
  (package
    (inherit guile-opengl)
    (inputs
     (map (match-lambda
            (("guile" _)
             `("guile" ,target-guile))
            (input input))
          (package-inputs guile-opengl)))
    (native-inputs
     (append (package-native-inputs guile-opengl)
             `(("autoconf" ,autoconf)
               ("automake" ,automake))))
    (arguments
     (substitute-keyword-arguments (package-arguments guile-opengl)
       ((#:phases phases)
        `(modify-phases ,phases
           (delete 'patch-makefile)
           (add-before 'bootstrap 'patch-configure.ac
             (lambda _
               ;; The Guile version check doesn't work for the 3.0
               ;; pre-release, so just remove it.
               (substitute* "configure.ac"
                 (("GUILE_PKG\\(\\[2.2 2.0\\]\\)") ""))
               (substitute* "Makefile.am"
                 (("\\$\\(GUILE_EFFECTIVE_VERSION\\)") "3.0")
                 (("ccache") "site-ccache"))
               #t))
           (replace 'bootstrap
             (lambda _
               (invoke "autoreconf" "-vfi")))))))))

(define guile-sdl2
  (let ((commit "2ecdf1e97f415a5818c4dcd7a2ddff6dc7ecdd0f"))
    (package
      (name "guile-sdl2")
      (version (string-append "0.5.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/guile-sdl2.git")
                      (commit commit)))
                (sha256
                 (base32
                  "0dzg321xr72x2xx49p93bdb44g6bx1hq8y39aqfzzh3qmdhyvhw5"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'bootstrap
                      (lambda _
                        (invoke "sh" "bootstrap"))))))
      (native-inputs
       `(("autoconf" ,autoconf)
         ("automake" ,automake)
         ("pkg-config" ,pkg-config)
         ("texinfo" ,texinfo)))
      (inputs
       `(("guile" ,target-guile)
         ("sdl2" ,sdl2)
         ("sdl2-image" ,sdl2-image)))
      (synopsis "Guile bindings for SDL2")
      (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
      (home-page "https://git.dthompson.us/guile-sdl2.git")
      (license license:lgpl3+))))

(define chickadee
  (let ((commit "3b4787975ec6684878db2b21dc33dcfdc748d51e"))
    (package
     (name "chickadee")
     (version (string-append "0.5.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/chickadee.git")
                    (commit commit)))
              (sha256
               (base32
                "1hlkmk903n3wg7ca9rf6fpdf4qj7gdvxdvj1vhczv4yqg6b6bjq6"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
                       (add-after 'unpack 'bootstrap
                                  (lambda _
                                    (invoke "sh" "bootstrap"))))))
     (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)
        ("texinfo" ,texinfo)))
     (inputs
      `(("freetype" ,freetype)
        ("guile" ,target-guile)
        ("libvorbis" ,libvorbis)
        ("mpg123" ,mpg123)
        ("openal" ,openal)))
     (propagated-inputs
      `(("guile-opengl" ,guile3.0-opengl)
        ("guile-sdl2" ,guile-sdl2)))
     (synopsis "Game development toolkit for Guile Scheme")
     (description "Chickadee is a game development toolkit for Guile
Scheme.  It contains all of the basic components needed to develop
2D/3D video games.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license license:gpl3+))))

(define starling
  (let ((commit "2e0780740a6e9515e76599cecb5a0cff7de196e6"))
    (package
     (name "starling")
     (version (string-append "0.1.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/starling.git")
                    (commit commit)))
              (sha256
               (base32
                "1ki3a3bxr09f7nqh0ri66nl376ws7q9isnghda1an5pbzk3s6wj5"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")
        #:phases
        (modify-phases %standard-phases
                       (add-after 'unpack 'bootstrap
                                  (lambda _
                                    (invoke "sh" "bootstrap"))))))
     (native-inputs
      `(("autoconf" ,autoconf)
        ("automake" ,automake)
        ("pkg-config" ,pkg-config)))
     (inputs
      `(("guile" ,target-guile)))
     (propagated-inputs
      `(("chickadee" ,chickadee)
        ("guile-sdl2" ,guile-sdl2)))
     (synopsis "Game engine for Guile Scheme")
     (description "Starling is a game engine that I haven't released
yet.")
     (home-page "https://git.dthompson.us/starling.git")
     (license license:gpl3+))))

(define %source-dir (dirname (current-filename)))

(package
  (name "spring-lisp-game-jam-2021")
  (version "0.1")
  (source (local-file %source-dir
                      #:recursive? #t
                      #:select? (git-predicate %source-dir)))
  (build-system gnu-build-system)
  (arguments
   `(#:modules (((guix build guile-build-system)
                 #:select (target-guile-effective-version))
                (ice-9 match)
                (ice-9 ftw)
                ,@%gnu-build-system-modules)
     #:imported-modules ((guix build guile-build-system) ,@%gnu-build-system-modules)
     #:make-flags '("GUILE_AUTO_COMPILE=0")
     #:phases
     (modify-phases %standard-phases
       (add-after 'unpack 'bootstrap
         (lambda _
           (invoke "sh" "bootstrap")))
       (add-after 'install 'wrap-script
         (lambda* (#:key inputs outputs #:allow-other-keys)
           (let* ((out  (assoc-ref outputs "out"))
                  (version (target-guile-effective-version))
                  (scm (string-append out "/share/guile/site/" version))
                  (go (string-append out "/lib/guile/" version "/site-ccache"))
                  (guile (which "guile")))
             (substitute* (string-append out "/bin/the-test-subject")
               (("exec guile" cmd)
                (string-append "export GUILE_LOAD_PATH=\""
                               scm ":"
                               (getenv "GUILE_LOAD_PATH")
                               "\"\n"
                               "export GUILE_LOAD_COMPILED_PATH=\""
                               go ":"
                               (getenv "GUILE_LOAD_COMPILED_PATH")
                               "\"\n"
                               "exec " guile)))))))))
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("pkg-config" ,pkg-config)))
  (inputs
   `(("guile" ,target-guile)))
  (propagated-inputs
   `(("starling" ,starling)))
  (synopsis "Spring Lisp Game Jam 2021 Entry")
  (description "Dave's Spring Lisp Game 2021 entry.")
  (home-page "https://git.dthompson.us/spring-lisp-game-jam-2021.git")
  (license license:gpl3+))
